# svugo

**svugo** is a minimal [Hugo](https://gohugo.io/) theme.

## Features

* Homepage with endless list of posts.
* Automatic Dark mode.
* [Multilingual mode](https://gohugo.io/content-management/multilingual/).
* No external dependencies, no JavaScript, no web fonts.

## Installation

To install `svugo`, download the repository into the `themes` folder in the root of your site.

```
git submodule add https://codeberg.org/smnvccrn/svugo.git themes/svugo
```

Then, use the theme to generate your site.

```
hugo server -t svugo
```

## Screenshots

### Light mode

![Screenshot](images/screenshot_light.png)

### Dark mode

![Screenshot](images/screenshot_dark.png)